async function check(name) {
  const url = 'https://mainnet.aptoslabs.com/v1/tables/0x21a0fd41330f3a0a38173c7c0e4ac59cd51505f0594f64d3d637c12425c3c155/item';
  const data = {
    key: {
      domain_name: name,
      subdomain_name: { vec: []},
    },
    key_type: '0x867ed1f6bf916171b1de3ee92849b8978b7d1b9e0a8cc982a3d19d535dfd9c0c::domains::NameRecordKeyV1',
    value_type: '0x867ed1f6bf916171b1de3ee92849b8978b7d1b9e0a8cc982a3d19d535dfd9c0c::domains::NameRecordV1',
  };

  const res = await fetch(url, {
    method: 'POST',
    headers: {
      'content-type': 'application/json',
    },
    body: JSON.stringify(data),
  }).catch(() => {});

  const result = { name };

  if (res?.status === 200) {
    result.status = 'reserved';
  } else if (res?.status === 404) {
    result.status = 'available';
  } else {
    result.status = 'error';
  }

  return result;
}

document.querySelector('form').addEventListener('submit', async (e) => {
  e.preventDefault();

  const submitButton = document.getElementById('submit-button');
  submitButton.disabled = true;
  submitButton.value = 'Checking...';

  const names = e.target.elements.names.value.trim().split(/\r\n|\n/);
  const results = await Promise.all(names.map(check));

  let html = '';
  for (const result of results) {
    html += `<div class="${result.status}"><a href="https://www.aptosnames.com/name/${result.name}" target="_blank">${result.name}</a></div>`;
  }

  submitButton.disabled = false;
  submitButton.value = 'Check';

  const output = document.getElementById('output');
  output.innerHTML = html + output.innerHTML;
});

document.getElementById('hide-reserved').addEventListener('change', (e) => {
  const output = document.getElementById('output');
  output.classList.toggle('hide-reserved', e.target.checked);
});
